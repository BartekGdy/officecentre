package Office;

/**
 * Created by RENT on 2017-08-31.
 */
public interface INotifiable {
    void notifyAbout(Object o);
}
