package Office;

import java.util.Optional;

/**
 * Created by RENT on 2017-08-31.
 */
public interface ITicketProvider {

    public Optional<Ticket> generateTicket(Gate gate, String registrationNumber);
    public boolean vehicleLeave(Gate gate, int ticketId, String registrationNumber);
}
