package Office;

import java.util.Optional;

/**
 * Created by RENT on 2017-08-31.
 */
public class Gate {
    private int gateId;
    private boolean isOpen;
    private ITicketProvider serwer;

    public Gate(int gateId, ITicketProvider serwer) {
        this.gateId = gateId;
        this.serwer = serwer;
    }

    private Optional<Ticket> generateTicket(String registrationNumber) {
        return serwer.generateTicket(this, registrationNumber);
    }

    public int checkIn(String registrationNumber) {
        Optional<Ticket> ticket = generateTicket(registrationNumber);
        if (ticket.isPresent()) {
            return ticket.get().getId();
        } else {
            return -1;
        }
    }

    public boolean checkOut(int ticketId, String registrationNumber) {
        return serwer.vehicleLeave(this, ticketId, registrationNumber);
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }
}
