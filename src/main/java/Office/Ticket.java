package Office;

/**
 * Created by RENT on 2017-08-31.
 */
 public class Ticket {
    private int id = 0;
    private long timestampIn;
    private long timestampOut;
    private long timestampPay;
    private boolean paid;
    private double amountToPay;
    private double converterInfo; // informacje o przeliczniku (przelicznik naliczania opłat za postój).

    public Ticket(long timestampIn, double converterInfo) {
        this.id = id + 1;
        this.timestampIn = timestampIn;
        this.converterInfo = converterInfo;
    }

    public void setTimestampOut(long timestampOut) {
        this.timestampOut = timestampOut;
    }

    public void setAmountToPay(double amountToPay) {
        this.amountToPay = amountToPay;
    }

    public int getId() {
        return id;
    }
}
