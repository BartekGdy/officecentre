package Office;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by RENT on 2017-08-31.
 */
class Parking {
    private int capacity;
    private Map<String, Ticket> parkedCars;
    private String name;

    public Parking(int capacity, String name) {
        this.capacity = capacity;
        this.name = name;
        this.parkedCars = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public int getCapacity() {
        return capacity;
    }

    public boolean parkingEntry(Ticket ticket, String registrationNumber) {
        if (isCarPresent(registrationNumber) && parkedCars.size() < capacity) {
            parkedCars.put(registrationNumber, ticket);
            System.out.println("Samochod zaparkował na parkingu " + this.name + " zostało miejsc " + (this.getCapacity() - this.parkedCars.size()) + "");
            return true;
        } else {
            System.out.println("Parking " + this.name + " jest pełen");
            return false;
        }
    }

    private boolean isCarPresent(String registrationNumber) {
        if (!parkedCars.containsKey(registrationNumber)) {
            return true;
        } else {
            System.out.println("Samochód o takiej rejestracji juz jest na parkingu, policja juz jedzie");
            return false;
        }
    }

    public boolean parkingLeave(Ticket ticket, String registrationNumber) {
        if (parkedCars.containsKey(registrationNumber) && parkedCars.get(registrationNumber).equals(ticket)) {
            parkedCars.remove(registrationNumber);
            System.out.println("Samochod wyjechał z parkingu " + this.name + " zostało miejsc " + (this.getCapacity() - this.parkedCars.size()) + "");
            return true;
        } else {
            System.out.println("Samochodu z ticketem" + ticket.getId() + " nie ma na parkingu");
            return false;
        }
    }
}
