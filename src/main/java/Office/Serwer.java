package Office;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by RENT on 2017-08-31.
 */
public class Serwer implements ITicketProvider {
    private Map<Integer, Ticket> tickets = new HashMap<>();
    private Map<String, Parking> parkings = new HashMap<>();
    private Map<Gate, Parking> gateParkingMap = new HashMap<>();

    @Override
    public Optional<Ticket> generateTicket(Gate gate, String registrationNumber) {
        Ticket ticket = new Ticket(Timestamp.valueOf(LocalDateTime.now()).getTime(), 1.0);
        if (gateParkingMap.get(gate).parkingEntry(ticket, registrationNumber)) {
            tickets.put(ticket.getId(), ticket);
            return Optional.of(ticket);
        }
        return Optional.empty();
    }

    @Override
    public boolean vehicleLeave(Gate gate, int ticketId, String registrationNumber) {
        if (gateParkingMap.get(gate).parkingLeave(tickets.get(ticketId), registrationNumber) && tickets.containsKey(ticketId)) {
            tickets.remove(ticketId);
            return true;
        } else {
            System.out.println("Nie ma takiego ticketa ");
            return false;
        }
    }

    public Parking addParking(String name, int capacity) {
        Parking parking = new Parking(capacity, name);
        parkings.put(parking.getName(), parking);
        System.out.println("Parking " + name + " dodany");
        return parking;
    }

    public Gate addGate(int gateId, String parkingName) {
        Gate gate = new Gate(gateId, this);
        gateParkingMap.put(gate, parkings.get(parkingName));
        System.out.println("Bramka " + gateId + " dodana");
        return gate;

    }
}
