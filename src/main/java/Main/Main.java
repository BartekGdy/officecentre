package Main;

import Office.*;

/**
 * Created by RENT on 2017-08-31.
 */
public class Main {
    public static void main(String[] args) {
        Olivia olivia = new Olivia();
        String parking1 = "Parking Dolny";
        String parking2 = "Parking Górny";

        olivia.getSerwer().addParking(parking1, 3);
        olivia.getSerwer().addParking(parking2, 2);
        Gate gate1 = olivia.getSerwer().addGate(1, parking1);
        Gate gate2 = olivia.getSerwer().addGate(2, parking2);


        int ticketID = gate1.checkIn("Test1");
        gate2.checkOut(ticketID, "Test1");
        ticketID = gate2.checkIn("Test2");
        gate1.checkOut(ticketID,"Cos");
        ticketID = gate2.checkIn("test3");
        int ticketID2 = gate1.checkIn("Test2");
        int ticketID3 = gate1.checkIn("Test3");
        int ticketID4 = gate1.checkIn("Test3");


    }
}
